from scipy.interpolate import splev, splrep
import operator as op
import matplotlib.pyplot as plt

from lvproblem import LVProblem
from lvsolver import LVSolver
from datacollector import DataCollector
from dataio import *
from fenics import *

def load_problem_from_passive(case, suffix = "",
                                    meshdir = "./meshes",
                                    datadir = "./data",
                                    resdir  = "./results"):

    info("\nLOADING SAVED VALUES FOR "+case)
    import cPickle
    outbase_passive = "{}/{}_passive{}".format(resdir, case, suffix)

    params = LVChallengeProblem.default_parameters()
    params["geometry"]["case"] = case
    params["geometry"]["meshdir"] = meshdir
    params["geometry"]["datadir"] = datadir
    
    problem = LVChallengeProblem(**params)
    problem.set_control_mode("pressure")
    
    # load the last step of passive inflation
    h5_load = HDF5File(mpi_comm_world(), 
            "{}_u_save.h5".format(outbase_passive), "r")
    
    h5_load.read(problem.state, "/initial")
    p_atrium = read_target_pressure(case, datadir, stage="ED")

    problem.set_pendo(p_atrium)
    control_parameter = None
    with open("{}_pv.cpickle".format(outbase_passive), "r") as f :
        data = cPickle.load(f)
        last_step = len(data["Vlist"]) - 1
        data.pop("Vlist")
        data.pop("plist")
        if data:
            control_parameter = data.keys()[0].replace("_list", "")
            control_value = data.values()[0][-1]
            info("Setting control parameter: {} = {}".format(\
                control_parameter, control_value))
            problem.set_control_parameters(**{control_parameter:control_value})
        
    h5_load.read(problem.state.vector(), "/values_{}".format(last_step), True)

    return problem

           
def iter_target(problem, collector, target_end, target_parameter="pressure",
                control_step=0.2, control_parameter="pressure",
                control_mode="pressure", tol=1e-4, adapt_step=True):
    """
    Takes an LVProblem to another state by stepwise changing one parameter.

    Arguments:
    ----------
    problem : LVProblem
        The problem containing the geometry and material laws
    collector : DataCollector
        An object to collect data during iterations
    target_end : float
        The target value we are iterating towards
    target_parameters : str
        The name of the target parameter
    control_step : float
        The size of the step the control value will be changed each iteration
    control_parameter : str
        The name of the control parameter
    control_mode : str
        Either 'pressure' or 'volume'. If 'pressure' we set the cavity
        'pressure' and solve for the volume. If 'volume' we set the
        cavity volume and solve for the pressure.
    tol : float
        Relative tolerance for reaching the target value.
    adapt_step : bool
        If True the control step can be expanded if the previous
        iteration converged in less than 6 newton iterations.
    """

    # The solver
    solver = LVSolver()

    # Set control mode and do an initial solve to check consistancy
    problem.set_control_mode(control_mode)
    solver.solve(problem)

    target_is_control = target_parameter == control_parameter

    # Sanity checks
    if control_parameter in ["pressure", "volume"] and control_parameter != control_mode:
        error("control mode and control parameter have to be the same.")

    if not target_is_control :
        if target_parameter not in ["volume", "pressure"]:
            error("expected target_parameter to be one of volume or pressure "\
                  "if different from control parameter")
        if target_parameter == control_mode:
            error("target parameter cannot be the same as the control mode")

    # Get present value of both target and control parameters
    target_value = problem.get_control_parameter(target_parameter)
    control_value = problem.get_control_parameter(control_parameter)

    # Get compare operator for checking when target value is reached (when control
    # and target is not the same parameter)
    comp = op.gt if target_value < target_end else op.lt

    info("\nITER TARGET {} {} {}".format(problem.parameters["geometry"]["case"],
                                         target_parameter, target_end))

    # Ouput present values of all control parameters
    collector.output_params()

    # Some flags
    target_reached = False

    # used during prediction
    target_values  = [ target_value ]
    control_values = [ control_value ]
    prev_states = [ problem.state.copy(True) ]

    # The main loop
    iterating = False
    while not target_reached:

        first_step = len(target_values) < 2
        
        # Old values
        target_value_old  = target_values[-1]
        control_value_old = control_values[-1]
        state_old = prev_states[-1]

        if target_is_control:

            # If we are close to the target
            if comp(target_value + control_step, target_end) :
                control_step = target_end - target_value
        else :

            # check if we cross the target
            # Interpolate!
            if not first_step :
                c0, c1 = control_values[-2:]
                t0, t1 = target_values[-2:]
                delta = (t1 - t0)/(c1 - c0)
                control_opt = 1/delta * (target_end - t0) + c0
                if iterating or abs(control_opt - c0) < abs(control_step):

                    # If we have enough values for a spline 
                    if len(target_values) >= 4:
                        # Sort and create a spline representation of the
                        # values and use that for interpolation/extrapolation
                        inds = np.argsort(target_values)
                        tck = splrep(np.array(target_values)[inds],
                                     np.array(control_values)[inds], k=1, s=0)
                        
                        new_control = float(splev(target_end, tck))
                        
                        control_step = new_control - control_value

                    # If not we do a linear interpolation/extrapolation
                    else:
                        control_step = control_opt - c0
                        control_value = c0

                    iterating = True

        # New control value
        control_value += control_step

        # prediction step
        # ---------------
        if not first_step :
            c0, c1 = control_values[-2:]
            s0, s1 = prev_states
            delta = (control_value - c0)/(c1 - c0)
            problem.state.vector().zero()
            problem.state.vector().axpy(1.0-delta, s0.vector())
            problem.state.vector().axpy(delta, s1.vector())
            #problem.state.vector().assign((1.0-delta)*s0 + delta*s1)

        # correction step
        # ---------------
        info("\nTRYING NEW CONTROL VALUE: {}={}\ntarget: {}={}, "\
             "control parameter: \"{}\", control_mode: \"{}\"".format(
                 control_parameter, control_value, target_parameter, \
                 target_end, control_parameter, control_mode))
        problem.set_control_parameters(**{control_parameter: control_value})
        try:
            nliter, nlconv = solver.solve(problem)
        except RuntimeError:

            info("\nNOT CONVERGING")

            # reset solution
            problem.state.assign(state_old)
            control_value = control_value_old

            # reduce step
            control_step *= 0.5
            info("REDUCING control_step = {}".format(control_step))
            continue

        # Get target value
        target_value = problem.get_control_parameter(target_parameter)

        # If we are going in the wrong direction
        if first_step and not iterating and abs(target_value - target_end) > \
               abs(target_value_old - target_end):

            info("\nSTEPING IN WRONG DIRECTION")
            
            # Reset solution
            problem.state.vector().zero()
            problem.state.vector().axpy(1.0, state_old.vector())
            #problem.state.assign(state_old)
            control_value = control_value_old
            
            # Reduce step
            control_step *= -1
            
            continue

        # Adapt control_step
        if not iterating and nliter < 7 and adapt_step :
            control_step *= 2.0
            info("\nINCREASING control_step = {}".format(control_step))

        # Check if target has been reached
        if abs(target_value - target_end) <= tol * target_end :
            target_str = "\nTARGET REACHED {} = {}".format(target_parameter, target_value)
            if target_parameter!=control_parameter:
                target_str += " WITH {} = {}".format(control_parameter, control_value)
            info(target_str)
            
            target_reached = True
            collector.save(save_vector=False)

        else:
            
            # Save state
            target_values.append(target_value)
            control_values.append(control_value)
            if first_step:
                prev_states.append(problem.state.copy(True))
            else:
                
                # Switch place of the state vectors
                prev_states = [prev_states[-1], prev_states[0]]

                # Inplace update of last state values
                prev_states[-1].vector().zero()
                prev_states[-1].vector().axpy(1.0, problem.state.vector())

            info("\nSUCCESFULL STEP:")
            collector.output_params()
            if not iterating and not comp(target_value, target_end):
                collector.save(save_vector=False)

def passive(problem, collector=None, volume_control_params=None, datadir='./data'):
    """
    Help function to expand the lv geometry passivly using pressure control mode.

    1) iterate to target pressure using pressure as control parameter
    2) if volume_control_params are passed, the function will also
       iterate the geometry to target volume (keeping pressure constant)
       using the passed volume_control_params as control parameter. 

    Target pressure and target volume will be read from disk using the
    stored case str in the Problem.

    Arguments:
    ----------
    problem : LVProblem
        The problem containing the geometry and material laws
    collector : DataCollector
        An object to collect data during iterations
    volume_control_params : list of str
        A list of parameters which will be used to fit the 
    datadir : str
        The path to where we have stored the data for each case.
    """

    # Do an initial solve to check consistancy if all passed parameters
    case = problem.parameters["geometry"]["case"]
    meshdir = problem.parameters["geometry"]["meshdir"]

    # Create a collector if not passed
    if collector is None:
        collector = DataCollector("{}_passive".format(case), problem, "pressure", resdir)

    # Read target pressure from file
    p_ED = read_target_pressure(case, datadir, "ED")

    # Pick an initial step size
    pstep = p_ED / 10

    # Do the first iteration reaching the target pressure
    iter_target(problem, collector, p_ED, target_parameter="pressure",
                control_step=pstep, control_parameter="pressure",
                control_mode="pressure")

    # If volume control parameter is passed we iterate towards ED volume
    if volume_control_params:
        V_ED = read_target_volume(case, meshdir, "ED")
        solver = LVSolver()
        solver.solve(problem)
        state_old = problem.state.copy(True)

        # Iterate over each volume control params
        # FIXME: Probably only work for a single controll parameter a time
        for control_param in volume_control_params:
            problem.state.assign(state_old)
            step = problem.get_control_parameter(control_param)/5.
            iter_target(problem, collector, V_ED, target_parameter="volume",
                        control_step=step, control_parameter=control_param,
                        control_mode="pressure")
            collector.dump_lists()

    collector.mark_stage("ED")
    collector.dump_lists()

def active(problem, collector=None, datadir='./data'):
    """
    Help function to expand the lv geometry actively by using
    gamma_fibers as control parameter. Two iterations are used to
    reach the pressure and volume in the ES stage. 

    1) keep volume constant (in volume control mode) while increasing pressure. 
    2) keep pressure constant (in pressure control mode) while increasing volume. 

    Target pressure and target volume will be read from disk using the
    stored case str in the Problem.

    Arguments:
    ----------
    problem : LVProblem
        The problem containing the geometry and material laws
    collector : DataCollector
        An object to collect data during iterations
    datadir : str
        The path to where we have stored the data for each case.
    """
    case = problem.parameters["geometry"]["case"]
    meshdir = problem.parameters["geometry"]["meshdir"]

    if collector is None:
        collector = DataCollector("{}_active".format(case), problem, "gamma_fibers", resdir)
        
    p_ES = read_target_pressure(case, datadir, "ES")
    V_ES = read_target_volume(case, meshdir, "ES")

    step = 0.01
    iter_target(problem, collector, p_ES, target_parameter="pressure",
                control_step=step, control_parameter="gamma_fibers",
                control_mode="volume")
    
    collector.mark_stage("EIV")
    iter_target(problem, collector, V_ES, target_parameter="volume",
                control_step=step, control_parameter="gamma_fibers",
                control_mode="pressure")
    
    collector.mark_stage("ES")
    
    collector.dump_lists()
    collector.plot([[read_target_volume(case, meshdir, "DS"), \
                     read_target_volume(case, meshdir, "ED"), V_ES],
                    [read_target_pressure(case, datadir, "DS"),
                     read_target_pressure(case, datadir, "ED"), p_ES]])

if __name__ == "__main__" :

    parameters["std_out_all_processes"] = False

    datadir = "./data"
    meshdir = "./meshes_p0.50"
    outdir  = "./results_p0.50"

    if mpi_comm_world().rank == 0:
        set_log_level(PROGRESS)
    else:
        set_log_level(ERROR)
    
    cases = [ '1024' ]

    # list of parameters to track
    plist = [ 'a_iso', 'b_iso', 'a_fibers', 'b_fibers', 'gamma_fibers' ]

    # hardcoded guess of material parameters from sensitivity
    pmopt = { '0912' : { 'a_iso' : 1.181, 'a_fibers' : 10.0 },
              '0917' : { 'a_iso' : 2.362, 'a_fibers' : 10.0 },
              '1017' : { 'a_iso' : 2.362, 'a_fibers' : 10.0 },
              '1024' : { 'a_iso' : 9.448, 'a_fibers' : 10.0 } }

    # default parameters
    params = LVProblem.default_parameters()
    params['geometry']['meshdir'] = meshdir
    params['geometry']['stage'] = 'DS'
    params['geometry']['fiber_space'] = 'Quadrature_4'

    # we select active strain model and we disable sheets
    params["material"]["active_model"] = "active_strain"
    params["material"]["a_sheets"] = 0.0
    params["material"]["b_sheets"] = 0.0
    params["material"]["a_cross"] = 0.0
    params["material"]["b_cross"] = 0.0

    # we optimize only wrt to a_fibers
    volume_control_parameters = [ "a_fibers" ]

    for case in cases:
        info("\nRUNNING CASE " + case)

        # update params
        params['geometry']['case'] = case
        for p, v in pmopt[case].items() :
            params['material'][p].assign(v)

        # instantiate the problem
        problem = LVProblem(**params)

        # collect data
        cname = '{}_full_cycle'.format(case)
        collector = DataCollector(cname, problem, plist, outdir)

        # Passive inflation
        passive(problem, collector, volume_control_parameters, datadir)

        # Active iso volumetric and isotonic contraction
        active(problem, collector, datadir)

        collector.move_files()

    plt.show()

from geometry import Geometry
from dataio import load_problem, select_problem_stage
from meshutils import *

# vtk is required
from vtk import *
from tvtk.array_handler import *
from meshutils import *
has_vtk = True
# check for old vtk
from distutils.version import StrictVersion
vtk6 = StrictVersion(vtkVersion.GetVTKVersion()) >= StrictVersion("6.0.0")

import numpy as np
import matplotlib.tri as tri
import matplotlib.pylab as plt

def vtk_snapshot(problem, stage, ofile, view='base') :
    geo_orig = problem.geo
    meshdir = geo_orig.parameters['meshdir']
    case = geo_orig.parameters['case']
    u = problem.state.sub(0, deepcopy=True)
    geo_cmp = Geometry(meshdir=meshdir, case=case, stage=stage)

    grid1 = dolfin2vtk(geo_orig.mesh, u)
    geo_cmp.bfun.array()[geo_cmp.bfun.array() == geo_cmp.BASE] = 0
    grid2 = dolfin2vtk(boundary_mesh(geo_cmp.mesh, geo_cmp.bfun))

    mapper1 = vtkDataSetMapper()
    mapper2 = vtkDataSetMapper()
    if vtk6:
        mapper1.SetInputData(grid1)
        mapper2.SetInputData(grid2)
    else:
        mapper1.SetInput(grid1)
        mapper2.SetInput(grid2)

    actor1 = vtkActor()
    actor2 = vtkActor()
    actor1.SetMapper(mapper1)
    actor2.SetMapper(mapper2)

    actor1.GetProperty().EdgeVisibilityOn()
    actor1.GetProperty().SetColor(0.9,0,0)
    actor1.GetProperty().SetEdgeColor(0.6,0,0)
    actor2.GetProperty().EdgeVisibilityOn()
    actor2.GetProperty().SetOpacity(0.6)
    actor2.GetProperty().SetEdgeColor(0.9,0.9,0.9)

    renderer = vtkRenderer()
    renderer.AddActor(actor1)
    renderer.AddActor(actor2)
    renderer.SetBackground(1,1,1)
    renderer.SetUseDepthPeeling(1)
    renderer.SetMaximumNumberOfPeels(6)

    camera = vtkCamera()
    if view == 'side' :
        camera.SetPosition(-3.4382, -7.7983, 8.04068)
        camera.SetFocalPoint(2.5333, 0.0809, -0.1856)
        camera.SetViewUp(-0.8856, 0.3162, -0.3400)
    else :
        camera.SetPosition(-10.3258, 0.1663, -0.3887)
        camera.SetFocalPoint(2.5339, 0.0809, -0.1856)
        camera.SetViewUp(0.0160, 0.6893, -0.7242)

    renderer.SetActiveCamera(camera)

    renWin = vtkRenderWindow()
    renWin.AddRenderer(renderer)
    renWin.SetAlphaBitPlanes(1)
    renWin.SetMultiSamples(0)

    renWinInteractor = vtkRenderWindowInteractor()
    renWinInteractor.SetRenderWindow(renWin)

    style = vtk.vtkInteractorStyleTrackballCamera()
    renWinInteractor.SetInteractorStyle(style)
    renWinInteractor.Initialize()

    #renWinInteractor.Start()

    image = vtkWindowToImageFilter()
    image.SetInput(renWin)
    png_writer = vtkPNGWriter()
    png_writer.SetInputConnection(image.GetOutputPort())
    png_writer.SetFileName(ofile)
    renWin.SetSize(800, 800)
    renWin.SetDPI(300)
    renWin.Render()
    png_writer.Write()

if __name__ == "__main__" :

    meshdir = "./meshes_p0.50"
    outdir  = "./results_p0.50"

    cases = [ '0912', '0917', '1017', '1024' ]
    stages = [ 'ED', 'ES' ]
    views = [ 'side', 'base' ]

    for case in cases :
        for stage in stages :
            problem = load_problem(meshdir, outdir, case, stage)
            u = problem.state.sub(0, deepcopy=True)
            for view in views :
                ofile = '{}_{}_{}.png'.format(case, stage, view)
                print "WRITING", ofile
                vtk_snapshot(problem, stage, ofile, view)

from fenics import *
import math as m
from textwrap import dedent

def subplus(u) :
    return conditional(ge(u, 0.0), u, 0.0)

def heaviside(u) :
    return conditional(ge(u, 0.0), 1.0, 0.0)

class ActiveHolzapfelOgden(object) :

    def __init__(self, **kwargs) :
        p = self.default_parameters()
        p.update(kwargs)
        self.parameters = p

    @staticmethod
    def default_parameters() :
        p = { 'units' : [ 1.0e+3, 1.0 ], # kPa
              'f0' : Constant((1.0, 0.0, 0.0), name="f0"),
              's0' : Constant((0.0, 1.0, 0.0), name="s0"),
              'a_iso'    : Constant(0.2362, name="a_iso"),
              'b_iso'    : Constant(10.810, name="b_iso"),
              'a_fibers' : Constant(20.037, name="a_fibers"),
              'b_fibers' : Constant(14.154, name="b_fibers"),
              'a_sheets' : Constant(3.7245),
              'b_sheets' : Constant(5.1645),
              'a_cross'  : Constant(0.4108),
              'b_cross'  : Constant(11.300),
              'active_model' : 'active_strain',

              # Active strain
              'gamma_fibers' : Constant(0.0),
              'anisotropic_ratio' : Constant(1.0),

              # Active stress
              'sigma_active' : Constant(0.0),
              'cross_fiber_proportion' : Constant(0.0),
            }
        return p

    def W1(self, I1) :
        """
        Isotropic contribution.
        """
        a = self.parameters['a_iso']
        b = self.parameters['b_iso']

        if float(a) > DOLFIN_EPS :
            if float(b) > DOLFIN_EPS :
                return a/(2.0*b) * (exp(b*(I1 - 3)) - 1.0)
            else :
                return a/2.0 * (I1 - 3)
        else :
            return 0.0

    def W4(self, I4, param_set) :
        """
        Anisotropic contribution.
        """
        a = self.parameters['a_{}'.format(param_set)]
        b = self.parameters['b_{}'.format(param_set)]

        if float(a) > DOLFIN_EPS :
            if float(b) > DOLFIN_EPS :
                return a/(2.0*b) * (exp(b*subplus(I4 - 1)**2) - 1.0)
            else :
                return a/2.0 * subplus(I4 - 1)**2
        else :
            return 0.0

    def W8(self, I8) :
        """
        Cross fiber-sheet contribution.
        """
        a = self.parameters['a_cross']
        b = self.parameters['b_cross']

        if float(a) > DOLFIN_EPS :
            if float(b) > DOLFIN_EPS :
                return a/(2.0*b) * (exp(b*I8**2) - 1.0)
            else :
                return a/2.0 * I8**2
        else :
            return 0.0

    def strain_energy(self, F, p) :
        """
        Total strain-energy density function.
        """

        f0 = self.parameters['f0']
        s0 = self.parameters['s0']

        # volumetric-isochoric decomposition
        C = F.T * F
        J = det(F)
        Jm23 = pow(J, -float(2)/3)

        # reduced invariants
        I1   = Jm23 * tr(C)
        I4f  = Jm23 * inner(C*f0, f0)
        I4s  = Jm23 * inner(C*s0, s0)
        I8fs = Jm23 * inner(C*f0, s0)

        # activation
        active_model = self.parameters['active_model']
        if active_model == 'active_stress' :
            sigma_active = self.parameters['sigma_active']
            raise NotImplemented
        elif active_model == 'active_strain' :
            gamma_f = self.parameters['gamma_fibers']
            mgamma_f = 1 - gamma_f
            # active invariants
            I1e   = mgamma_f * I1 + (1/mgamma_f**2 - mgamma_f) * I4f
            I4fe  = 1/mgamma_f**2 * I4f
            I4se  = mgamma_f * I4s
            I8fse = 1/sqrt(mgamma_f) * I8fs
            # strain energy
            W = self.W1(I1e) \
              + self.W4(I4fe, 'fibers') \
              + self.W4(I4se, 'sheets') \
              + self.W8(I8fse)

        return W - p * (J - 1)


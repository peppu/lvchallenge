from fenics import *
from fenicshotools.gmsh import geo2dolfin
from textwrap import dedent
import numpy as np
from scipy.spatial import cKDTree
from scipy.interpolate import Rbf
import os.path

def create_geometry(meshdir, case, stage, mesh_scaling=1.0) :
    geocode = dedent(\
    """\
    // meshing options
    Mesh.CharacteristicLengthFromCurvature = 1;
    Mesh.Lloyd = 1;
    Mesh.CharacteristicLengthMin = 3.0;
    Mesh.CharacteristicLengthMax = 4.0;
    Mesh.Optimize = 1;
    Mesh.OptimizeNetgen = 1;
    Mesh.RemeshParametrization = 7;
    Mesh.SurfaceFaces = 1;
    Mesh.CharacteristicLengthFactor = {mesh_scaling};

    // load the surfaces
    Merge "{meshdir}/{case}{stage}Endo_cut.ply";
    Merge "{meshdir}/{case}{stage}Epi_cut.ply";

    CreateTopology;

    ll[] = Line "*";
    L_LV_base = newl; Compound Line(L_LV_base) = ll[1];
    L_epi_base = newl; Compound Line(L_epi_base) = ll[0];
    Physical Line("ENDORING") = {{ L_LV_base }};
    Physical Line("EPIRING") = {{ L_epi_base }};

    ss[] = Surface "*";
    S_LV = news; Compound Surface(S_LV) = ss[0];
    S_epi = news; Compound Surface(S_epi) = ss[1];
    Physical Surface("ENDO") = {{ S_LV }};
    Physical Surface("EPI") = {{ S_epi }};

    LL_base = newll; 
    Line Loop(LL_base) = {{ L_LV_base, L_epi_base }};
    S_base = news; Plane Surface(S_base) = {{ LL_base }};
    Physical Surface("BASE") = {{ S_base }};

    SL_wall = newsl; 
    Surface Loop(SL_wall) = {{ S_LV, S_epi, S_base }};
    V_wall = newv; Volume(V_wall) = {{ SL_wall }};
    Physical Volume("MYOCARDIUM") = {{ V_wall }};
    """).format(stage=stage, case=case, meshdir=meshdir,
                mesh_scaling=mesh_scaling)

    mesh, names, _ = geo2dolfin(geocode)

    return mesh, names

def interpolate_fibers(V, meshdir, case, stage) :
    fname = "{}/{}{}_fibers_orig.axis".format(meshdir, case, stage)

    xyz, forig = np.split(np.genfromtxt(fname, skip_header=1), 2, axis=1)
    tree = cKDTree(xyz)

    # coordinate of the dofs
    idx = np.column_stack([ V.sub(i).dofmap().dofs() 
               for i in xrange(0, 3) ])
    coords = V.dofmap().tabulate_all_coordinates(V.mesh()).reshape(-1, 3)

    f0 = Function(V)

    nsamples = 10
    for vidx in idx :
        v = coords[vidx[0],:]
        #print tree.query_ball_point(v, radius)
        samples_rad, samples_idx = tree.query(v, nsamples)
        xx, yy, zz = np.split(xyz[samples_idx,:], 3, axis=1)
        fx, fy, fz = np.split(forig[samples_idx,:], 3, axis=1)

        # FIXME rbf with values on sphere or least-squares with constraint
        rbf = [ Rbf(xx, yy, zz, ff, function='gaussian', epsilon=samples_rad)
                for ff in [ fx, fy, fz ] ]
        fi = np.array([ f(v[0], v[1], v[2]) for f in rbf ])
        fi /= np.linalg.norm(fi)

        f0.vector()[vidx[0]] = fi[0]
        f0.vector()[vidx[1]] = fi[1]
        f0.vector()[vidx[2]] = fi[2]

    return f0

if __name__ == "__main__" :
    # if true we get all meshes and fibers in vtk format
    export_vtk = False
    # rescaling factor for the elements
    scaling = 0.5
    # input .ply and fibers
    inmeshdir  = os.path.abspath("./data/raw_meshes")
    # output directory
    outmeshdir = os.path.abspath("./meshes_p{:.2f}".format(scaling))

    cases  = [ "1024", "1017", "0912", "0917" ]
    stages = [ "DS", "ED", "ES" ]

    for case in cases :
        h5name = "{}/{}.h5".format(outmeshdir, case)
        h5file = HDF5File(mpi_comm_world(), h5name, "w")
        for stage in stages :
            info("\n\nSTAGE {}".format(stage))
            info("\nGENERATING GEOMETRY ...")
            mesh, names = create_geometry(inmeshdir, case, stage, scaling)
            # markers
            bfun = MeshFunction("size_t", mesh, 2, mesh.domains())
            bfun.array()[bfun.array() == max(bfun.array())] = 0
            rfun = MeshFunction("size_t", mesh, 1, mesh.domains())
            rfun.array()[rfun.array() == max(rfun.array())] = 0
            mesh.domains().clear()

            f0list = {}
            if stage == "DS" :
                info("\nINTERPOLATE FIBERS ...")
                Vlist = [ VectorFunctionSpace(mesh, "Quadrature", 2),
                          VectorFunctionSpace(mesh, "Quadrature", 4),
                          VectorFunctionSpace(mesh, "P", 1),
                          VectorFunctionSpace(mesh, "P", 2) ]
                for V in Vlist :
                    e = V.ufl_element()
                    name = "{}_{}".format(e.family(), e.degree())
                    info("SPACE {}".format(name))
                    f0list[name] = interpolate_fibers(V, inmeshdir, case, stage)

            info("\nRESCALING GEOMETRY ...")
            mesh.coordinates()[:] /= 10.0

            info("\nSAVE GEOMETRY ...")
            h5file.write(mesh, "{}/mesh".format(stage))
            bgroup = "{}/facets markers".format(stage)
            h5file.write(bfun, bgroup)
            h5file.attributes(bgroup)["base"] = names["BASE"]
            h5file.attributes(bgroup)["endo"] = names["ENDO"]
            h5file.attributes(bgroup)["epi"] = names["EPI"]
            rgroup = "{}/ridges markers".format(stage)
            h5file.write(rfun, rgroup)
            h5file.attributes(rgroup)["endoring"] = names["ENDORING"]
            h5file.attributes(rgroup)["epiring"] = names["EPIRING"]

            for name, f0 in f0list.items() :
                h5file.write(f0, "{}/fibers at {}".format(stage, name))

            if not export_vtk : continue

            info("\nSAVE XDMF FILES ...")
            File("{}/{}{}.xdmf".format(outmeshdir, case, stage)) << mesh
            if len(f0list) > 0 :
                f0 = f0list["Lagrange_1"]
                File("{}/{}{}_fibers.xdmf".format(outmeshdir, case, stage)) << f0

        h5file.close()

Source for the 'STACOM 2014 LV Challenge', based on FEniCS.

See:
Patient–Specific Parameter Estimation for a Transversely Isotropic
Active Strain Model of Left Ventricular Mechanics
S Gjerald, J Hake, S Pezzuto, J Sundnes, ST Wall

http://link.springer.com/chapter/10.1007/978-3-319-14678-2_10

Requirements
------------
* FEniCS 1.5.0
* VTK
* Mayavi

Remarks
-------
* The main code for the mechanical solver starts in ```itertarget.py```.
* Post-processing is in ```globalquantities.py```.

Authors
-------
* Johan Hake
* Simone Pezzuto

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math as m
import scipy.interpolate as interpolate

matplotlib.rcParams['xtick.direction'] = 'out'
matplotlib.rcParams['ytick.direction'] = 'out'
matplotlib.rcParams['text.usetex'] = True

def resample(X, Y, Z, xmin, xmax, ymin, ymax, nx, ny) :
    spline = interpolate.RectBivariateSpline(X[:,0], Y[0,:], Z, s=0)
    #spline = interpolate.interp2d(X[:,0], Y[0,:], Z, kind='linear')
    xref = np.linspace(xmin, xmax, nx)
    yref = np.linspace(ymin, ymax, ny)
    Xref, Yref = np.meshgrid(xref, yref)
    Zref = spline(xref, yref).transpose()
    return Xref, Yref, Zref

if __name__ == "__main__" :
    resdir = './results_sensitivity'
    case = '1024'
    plist = [ 'a_iso', 'a_fibers' ]

    ifile = '{}/{}_{}_{}.txt'.format(resdir, case, *plist)
    data = np.genfromtxt(ifile, names=True)

    n1 = len(np.unique(data[plist[0]]))
    n2 = len(np.unique(data[plist[1]]))

    X = data[plist[0]].reshape(n2, n1).transpose()
    Y = data[plist[1]].reshape(n2, n1).transpose()

    fig, axs = plt.subplots(3, 2)
    fig.set_size_inches(10., 12.)

    # volume
    # ------
    Z = data['vol'].reshape(n2, n1).transpose()
    ref = data['volref'][0]
    # resampling
    Xref, Yref, Zref = resample(X, Y, Z, 1.0, 30.0, 1.0, 100.0, 1000, 1000)

    levels = np.array([17.5, 18.5, 19.5, 20.5, 21.5, 22.5 ])
    labels = [ '${:.1f}$'.format(v) for v in levels ]
    labels[0] = '$< {:.1f}$'.format(levels[0])
    labels[-1] = '$> {:.1f}$'.format(levels[-1])
    cs1 = axs[0,0].contourf(Xref, Yref, np.clip(Zref, 17.5, 22.5),
                       30,
                       cmap=matplotlib.cm.coolwarm)
    cs2 = axs[0,0].contour(Xref, Yref, Zref,
                      levels=[ref],
                      colors='k', hold='on')
    cbar = fig.colorbar(cs1, ax=axs[0,0], ticks=levels)
    cbar.ax.set_yticklabels(labels)
    #fmt = { ref: '$\mathsf{Target\;Volume}$' }
    #axs[0].clabel(cs2, cs2.levels, fontsize=12, inline=True, fmt=fmt,
    #              manual=[(15.0, 50.0)])
    plt.setp(cs2.collections[0], linewidth=2)
    axs[0,0].set_xlabel('$a\;\mathrm{[kPa]}$')
    axs[0,0].set_ylabel('$a_\mathsf{f}\;\mathrm{[kPa]}$')
    axs[0,0].set_title('$\mathsf{ED\;Volume\;[ml]}$')

    # thickness
    # ---------
    Z = data['thick'].reshape(n2, n1).transpose()
    ref = data['thickref'][0]
    # resampling
    Xref, Yref, Zref = resample(X, Y, Z, 1.0, 30.0, 1.0, 100.0, 1000, 1000)

    levels = np.arange(0.86,0.98,0.03)
    labels = [ '${:.2f}$'.format(v) for v in levels ]
    cs1 = axs[0,1].contourf(Xref, Yref, Zref, 30, cmap=matplotlib.cm.coolwarm)
    cs2 = axs[0,1].contour(Xref, Yref, Zref,
                      levels=[ref],
                      colors='k', hold='on')
    cbar = fig.colorbar(cs1, ax=axs[0,1], ticks=levels)
    cbar.ax.set_yticklabels(labels)
    plt.setp(cs2.collections[0], linewidth=2)
    axs[0,1].set_xlabel('$a\;\mathrm{[kPa]}$')
    axs[0,1].set_ylabel('$a_\mathsf{f}\;\mathrm{[kPa]}$')
    axs[0,1].set_title('$\mathsf{ED\;Thickness\;[cm]}$')

    # length
    # ------
    ax = axs[1,0]
    Z = data['length'].reshape(n2, n1).transpose()
    ref = data['lengthref'][0]
    # resampling
    Xref, Yref, Zref = resample(X, Y, Z, 1.0, 30.0, 1.0, 100.0, 1000, 1000)

    levels = np.arange(5.2,5.5,0.03)
    labels = [ '${:.2f}$'.format(v) for v in levels ]
    cs1 = ax.contourf(Xref, Yref, Zref, 30, cmap=matplotlib.cm.coolwarm)
    cs2 = ax.contour(Xref, Yref, Zref,
                      levels=[ref],
                      colors='k', hold='on')
    cbar = fig.colorbar(cs1, ax=ax, ticks=levels)
    cbar.ax.set_yticklabels(labels)
    ax.set_xlabel('$a\;\mathrm{[kPa]}$')
    ax.set_ylabel('$a_\mathsf{f}\;\mathrm{[kPa]}$')
    ax.set_title('$\mathsf{ED\;Length\;[cm]}$')

    # distepi
    # -------
    ax = axs[1,1]
    Z = data['distepi'].reshape(n2, n1).transpose()
    # resampling
    Xref, Yref, Zref = resample(X, Y, Z, 1.0, 30.0, 1.0, 100.0, 1000, 1000)

    levels = np.arange(0.03,0.25,0.03)
    labels = [ '${:.2f}$'.format(v) for v in levels ]
    cs1 = ax.contourf(Xref, Yref, Zref, 30, cmap=matplotlib.cm.coolwarm)
    cbar = fig.colorbar(cs1, ax=ax, ticks=levels)
    cbar.ax.set_yticklabels(labels)
    ax.set_xlabel('$a\;\mathrm{[kPa]}$')
    ax.set_ylabel('$a_\mathsf{f}\;\mathrm{[kPa]}$')
    ax.set_title('$\mathsf{ED\;Epi\;Distance\;[cm]}$')

    # distepi
    # -------
    ax = axs[2,0]
    Z = data['distendo'].reshape(n2, n1).transpose()
    # resampling
    Xref, Yref, Zref = resample(X, Y, Z, 1.0, 30.0, 1.0, 100.0, 1000, 1000)

    levels = np.arange(0.03,0.25,0.03)
    labels = [ '${:.2f}$'.format(v) for v in levels ]
    cs1 = ax.contourf(Xref, Yref, Zref, 30, cmap=matplotlib.cm.coolwarm)
    cbar = fig.colorbar(cs1, ax=ax, ticks=levels)
    cbar.ax.set_yticklabels(labels)
    ax.set_xlabel('$a\;\mathrm{[kPa]}$')
    ax.set_ylabel('$a_\mathsf{f}\;\mathrm{[kPa]}$')
    ax.set_title('$\mathsf{ED\;Endo\;Distance\;[cm]}$')

    axs[2,1].axis('off')

    plt.subplots_adjust(left=0.1, right=0.98, hspace=0.34,
                        bottom=0.05, top=0.98, wspace=0.30)
    plt.savefig('test.pdf')
    plt.show()

from geometry import Geometry
from holzapfelogden import ActiveHolzapfelOgden
from edgetypebc import *
from realspaces import *
from fenics import *

class LVProblem(NonlinearProblem) :
    def __init__(self, **kwargs) :
        super(LVProblem, self).__init__()

        params = self.default_parameters()
        params.update(**kwargs)
        self.parameters = params

        # geometry
        ffc_par = self.parameters['form_compiler_parameters']
        geo_par = self.parameters['geometry']
        geo_par['fibers_quadrature_degree'] = ffc_par['quadrature_degree']
        self.geo = Geometry(**geo_par)

        # material (setting fibers)
        mat_par = self.parameters['material']
        mat_par['f0'] = self.geo.f0
        self.mat = ActiveHolzapfelOgden(**mat_par)

        self._init_spaces()
        self.state = Function(self._Mspace)
        self._init_forms()
        self._prev_residual = 1.
        self._recompute_jacobian = True
        self._first_iteration = True

        #print [str(c) for c in self._G.coefficients()]
        #for n, p in self.mat.parameters.items():
        #    print n, str(p)

    @staticmethod
    def default_parameters() :
        p = { 'form_compiler_parameters' : { 
              'quadrature_degree' : 4,
              'cpp_optimize' : True,
              'precompute_basis_const' : True,
              'optimize' : True,
              'cpp_args' : "-O3 --fast-math",
            },
              'output_dir' : 'results/passive',
              'bc_type' : 'fix_base_ver',
              'control_volume' : False,
              'geometry' : Geometry.default_parameters(),
              'material' : ActiveHolzapfelOgden.default_parameters()}
        return p

    def set_control_mode(self, control_mode):
        assert control_mode in ["pressure", "volume"]
        self._change_mode_and_reinit(control_volume = control_mode == "volume")

    def get_control_mode(self):
        return "volume" if self.parameters['control_volume'] else "pressure"

    def F(self, b, x):
        log(PROGRESS, "\nAssemble F")
        ffc_params = self.parameters['form_compiler_parameters']
        assemble(self._G, tensor = b, \
                 form_compiler_parameters = ffc_params)

        if self._bcs : self._bcs.apply(b)
        
        # Compute residual
        residual = b.norm("l2")
        residual_ratio = residual/self._prev_residual
        self._recompute_jacobian = residual_ratio > 0.3
        if not self._first_iteration:
            info("residual: {:e} ".format(residual)+ \
                 "previous residual: {:e} ".format(self._prev_residual)+ \
                 "ratio: {:e}".format(residual_ratio))
        self._prev_residual = residual

    def J(self, A, x) :
        ffc_params = self.parameters['form_compiler_parameters']
        if self._first_iteration or self._recompute_jacobian:
            log(PROGRESS, "\nAssemble J")
            assemble(self._dG, tensor=A, \
                     form_compiler_parameters=ffc_params)
            if self._bcs : self._bcs.apply(A)
            self._first_iteration = False

    def set_pendo(self, p) :
        param = self.parameters
        mat = self.mat
        if param['control_volume']:
            pnum = self._Mspace.num_sub_spaces() - 1
            #set_realspace_var(self.state, pnum, mat.to_ref_units(p))
            set_realspace_var(self.state, pnum, p)
        else :
            #param['p_endo'].assign(mat.to_ref_units(p))
            param['p_endo'].assign(p)

    def set_Vendo(self, V) :
        param = self.parameters
        if not param['control_volume'] :
            raise RuntimeError('Cannot assign Vendo!')
        else :
            param['V_endo'].assign(V)

    def get_pendo(self) :
        param = self.parameters
        if param['control_volume'] :
            pnum = self._Mspace.num_sub_spaces() - 1
            return get_realspace_var(self.state, pnum)
        else :
            return float(param['p_endo'])

    def get_Vendo(self) :
        param = self.parameters
        if not param['control_volume'] :
            geo = self.geo
            u = split(self.state)[0]
            return geo.inner_volume(u)
        else :
            return float(param['V_endo'])

    def set_control_parameters(self, **kwargs):
        
        debug("setting params: {}".format(", ".join("{}:{}".format(n, i) \
                                                    for n, i in kwargs.items())))
        for param, value in kwargs.items():

            if param in ["pressure", "volume"]:
                if param == "pressure":
                    if self.parameters['control_volume']:
                        error("Problem is in volume control mode. Cannot use "\
                              "pressure as control.")
                    self.set_pendo(value)
                else:
                    if not self.parameters['control_volume']:
                        error("Problem is in pressure control mode. Cannot use "\
                              "volume as control.")
                    self.set_Vendo(value)
            else:
                if param not in self.mat.parameters:
                    error("{} is not a parameters.".format(param))

                self.mat.parameters[param].assign(value)

    def get_control_parameter(self, param):
        if param in ["pressure", "volume"]:
            if param == "pressure":
                return self.get_pendo()
            
            else:
                return self.get_Vendo()

        if param not in self.mat.parameters:
            error("{} is not a control parameter.".format(param))

        return float(self.mat.parameters[param])

    def get_displacement(self) :
        return self.state.split()[0]

    def _init_forms(self) :
        geo = self.geo
        mat = self.mat
        domain = geo.domain

        param = self.parameters

        # unknowns
        z = self.state
        u = split(z)[0]
        p = split(z)[1]
        
        if param['control_volume'] :
            pendo = split(z)[-1]
            param['V_endo'] = Constant(geo.inner_volume())
            Vendo = param['V_endo']
        else :
            param['p_endo'] = Constant(0.0)
            pendo = param['p_endo']
            Vendo = None

        # internal energy
        # ---------------
        X = SpatialCoordinate(domain)
        x = X + u
        F = grad(x)
        Lmat = mat.strain_energy(F, p) * dx

        # inner pressure
        # --------------
        Lvol = self._inner_volume_constraint(u, pendo, Vendo, geo.bfun, geo.ENDO)

        # bcs
        # ---
        if param['bc_type'] == 'fix_endoring' :
            # no rigid motions
            Lrig = 0
            # fix the endocardium ring in all directions
            endoring = pick_endoring_bc()(geo.rfun, geo.ENDORING)
            Vsp = self._Mspace.sub(0)
            val = Constant((0.0, 0.0, 0.0))
            bcs = DirichletBC(Vsp, val, endoring, method = "pointwise")
        elif param['bc_type'] == 'fix_base_ver' :
            c = split(z)[2]
            # no traslations in yz-plane
            ct = as_vector([ 0.0, c[0], c[1] ])
            # no rotations around x-axis
            cr = as_vector([ c[2], 0.0, 0.0 ])
            Lrig = self._rigid_motion_constraints(u, ct, cr)
            # no vertical displacement at the base
            Vsp = self._Mspace.sub(0).sub(0)
            bcs = DirichletBC(Vsp, Constant(0.0), geo.bfun, geo.BASE)
        elif param['bc_type'] == 'free' :
            # 6 rigid motions to be removed
            raise NotImplemented
        else:
            raise RuntimeError('Invalid bc type!')

        # tangent problem
        # ---------------
        L = Lmat + Lvol + Lrig

        self._G   = derivative(L, z, TestFunction(self._Mspace))
        self._dG  = derivative(self._G, z, TrialFunction(self._Mspace))
        self._bcs = bcs

    def _init_spaces(self) :
        domain = self.geo.domain

        vlist = [ VectorFunctionSpace(domain, "CG", 2),
                  FunctionSpace(domain, "CG", 1) ]

        vlist += {
            'fix_endoring' : [],
            'fix_base_ver' : [ VectorFunctionSpace(domain, "Real", 0, 3) ],
            'free'         : [ VectorFunctionSpace(domain, "Real", 0, 6) ],
            }[self.parameters['bc_type']]

        if self.parameters['control_volume'] :
            vlist += [ FunctionSpace(domain, "Real", 0) ]

        self._Mspace = MixedFunctionSpace(vlist)

    @staticmethod
    def _rigid_motion_constraints(u, ct, cr) :
        """
        Compute the form
            (u, ct) * dx + (cross(u, X), cr) * dx
        where
            u  = displacement
            ct = Lagrange multiplier for translations
            ct = Lagrange multiplier for rotations
        """

        dom = u.domain()
        dim = dom.geometric_dimension()
        X = SpatialCoordinate(dom)

        Lt = inner(ct, u) * dx

        if dim == 2 :
            # rotations around z
            Lr = inner(cr, X[0]*u[1] - X[1]*u[0]) * dx
        elif dim == 3:
            # rotations around x, y, z
            Lr = inner(cr, cross(X, u)) * dx

        return Lt + Lr

    @staticmethod
    def _inner_volume_constraint(u, p, V, bfun, sigma) :
        """
        Compute the form
            (V(u) - V, p) * ds(sigma)
        where V(u) is the volume computed from u and
            u = displacement
            V = volume enclosed by sigma
            p = Lagrange multiplier
        sigma is the boundary of the volume.
        """

        dom = u.domain()
        X = SpatialCoordinate(dom)
        N = FacetNormal(dom)
        # ufl doesn't support any measure for duality
        # between two Real spaces, so we have to divide
        # by the total measure of the domain
        ds_sigma = ds(sigma, domain = dom, subdomain_data = bfun)
        area = assemble(Constant(1.0) * ds_sigma)

        # in detail, we impose the constraint
        #   ( 1/dim (x, n) + 1/area V ) * p = 0
        x = X + u
        F = grad(x)
        n = cofac(F)*N

        V_u = - Constant(1.0/3.0) * inner(x, n)
        L = - p * V_u * ds_sigma

        if V is not None :
            L += Constant(1.0/area) * p * V * ds_sigma

        return L

    def _change_mode_and_reinit(self, control_volume = False):
        if self.parameters['control_volume'] == control_volume:
            return

        # save the current state
        state_old = self.state.copy(True)
        pendo_old = self.get_pendo()
        Vendo_old = self.get_Vendo()

        # reinit problem
        self.parameters['control_volume'] = control_volume
        self._init_spaces()
        self.state = Function(self._Mspace)
        self._init_forms()

        # assign old values
        assign(self.state.sub(0), state_old.sub(0))
        assign(self.state.sub(1), state_old.sub(1))
        if self.parameters['bc_type'] != 'fix_endoring' :
            assign(self.state.sub(2), state_old.sub(2))
        self.set_pendo(pendo_old)
        if control_volume :
            self.set_Vendo(Vendo_old)


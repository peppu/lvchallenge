from fenics import *
from lvproblem import LVProblem
from lvsolver import LVSolver
from geometry import Geometry
from meshutils import surf_distance

import dataio
import numpy as np
import os.path
import cPickle

from scipy.spatial import cKDTree

import matplotlib.pyplot as plt

class ProblemEvaluator(object) :
    def __init__(self, meshdir, datadir, outdir, case, plist) :
        # initialization
        param = LVProblem.default_parameters()
        param['geometry']['meshdir'] = meshdir
        param['geometry']['case'] = case
        param['geometry']['stage'] = 'DS'
        param['geometry']['fibers_space'] = 'Quadrature_4'
        param['material']['active_model'] = 'active_strain'
        # always disable sheet- and cross-term
        param['material']['a_sheets'] = 0.0
        param['material']['b_sheets'] = 0.0
        param['material']['a_cross'] = 0.0
        param['material']['b_cross'] = 0.0

        self._problem = LVProblem(**param)
        self._problem.set_control_mode("pressure")

        if len(plist) > 2 :
            error("Maximum 2 parameters allowed")
        self._controls = plist

        # data-structure to cache solutions
        comm = self._problem.geo.mesh.mpi_comm()
        h5name = "{}/{}_{}.h5".format(outdir, case, "_".join(plist))
        self._pickle = "{}/{}_{}.pickle".format(outdir, case, "_".join(plist))
        if os.path.isfile(h5name) and os.path.isfile(self._pickle) :
            self._h5db = HDF5File(comm, h5name, "a")
            self._data = cPickle.load(open(self._pickle, "r"))
        else :
            self._h5db = HDF5File(comm, h5name, "w")
            self._data = { k: [] for k in plist }

        # target pressure
        self._pED = dataio.read_target_pressure(case, datadir, "ED")

    def bootstrap(self, **pvals) :
        # bootstrap of the problem
        pED = self._pED
        info("\nBOOTSTRAP PROBLEM WITH pED = {}".format(pED))
        params = { k: pvals[k] for k in self._controls }
        self._problem.set_control_parameters(**params)
        self._solver = LVSolver()
        if len(self._data[self._controls[0]]) == 0 :
            self._solver.solve(self._problem)
            self._simple_itertarget("pressure", pED, cstep = pED/10.0)
            self._store()
        else :
            self._problem.set_pendo(pED)
            self.__call__(**pvals)
            self._solver.solve(self._problem)

    def _simple_itertarget(self, control, cend, cstep = None) :
        pb = self._problem
        cval = pb.get_control_parameter(control)
        cstep = cstep or cend - cval
        # FIXME check direction!
        # FIXME save all the steps
        while cval < cend :
            cold = cval
            sold = pb.state.copy(True)
            cval += cstep
            pb.set_control_parameters(**{control: cval})
            try :
                info("\nSOLVING FOR {} = {}".format(control, cval))
                nliter, nlconv = self._solver.solve(pb)
            except RuntimeError :
                pb.state.assign(sold)
                cval = cold
                cstep /= 2.0
                info("!!! REDUCING STEP = {}".format(cstep))
                continue

            if nliter < 10 :
                cstep *= 1.5
            if cval + cstep >= cend :
                cstep = cend - cval

    def _store(self) :
        pb = self._problem
        for p in self._controls :
            self._data[p] += [ pb.get_control_parameter(p) ]
            idx = len(self._data[p]) - 1
        info("STORING SOLUTION {}".format(idx))
        h5group = "/state_{}".format(idx)
        self._h5db.write(self._problem.state, h5group)
        for p in self._controls :
            self._h5db.attributes(h5group)[p] = self._data[p][idx]
        # overwrite the whole pickle database
        cPickle.dump(self._data, open(self._pickle, "w"))
 
    def _load(self, idx, check_parameters = False) :
        h5group = "/state_{}".format(idx)
        # check for the right parameters
        pb = self._problem
        if not self._h5db.has_dataset(h5group) :
            return False
        retval = True
        for p in self._controls :
            retval = retval and self._data[p][idx] == pb.get_control_parameter(p)
        # loading
        retval = retval if check_parameters else True
        if retval :
            info("LOADING SOLUTION {}".format(idx))
            self._h5db.read(self._problem.state, h5group)

        return retval

    def __call__(self, tol=1e-8, **pvals) :
        # look for a good guess of the state
        pspace = np.array([ self._data[p] for p in self._controls ]).transpose()
        tree = cKDTree(pspace)
        dist, idx = tree.query(np.array([pvals[p] for p in self._controls]), 1)
        self._load(idx)
        # solve
        info("\nSOLVING FOR {}".format(\
             ", ".join("{} = {}".format(p, v) for p, v in pvals.items())))
        if dist > tol :
            self._problem.set_control_parameters(**pvals)
            self._solver.solve(self._problem)
            self._store()

        return self.compare_reference_geometry()

    def compare_reference_geometry(self) :
        if MPI.size(mpi_comm_world()) > 1 :
            return [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
        # reference values
        if not hasattr(self, '_georef') :
            meshdir = self._problem.parameters['geometry']['meshdir']
            case = self._problem.parameters['geometry']['case']
            self._georef = Geometry(meshdir=meshdir, case=case, stage='ED')
        if not hasattr(self, '_volref') :
            self._volref = self._georef.inner_volume()
        if not hasattr(self, '_lengthref') :
            self._lengthref = self._georef.ventricle_axial_length('epi')
        if not hasattr(self, '_thick') :
            r_epi = self._georef.average_radius('epi', 1.0)
            r_endo = self._georef.average_radius('endo', 1.0)
            self._thickref = r_epi - r_endo
        # actual values
        u = self._problem.state.sub(0, deepcopy=True)
        geo = self._problem.geo
        vol = geo.inner_volume(u)
        length = geo.ventricle_axial_length('epi', u)
        thick = geo.average_radius('epi', 1.0, u) \
              - geo.average_radius('endo', 1.0, u)
        # surface distance
        distepi  = surf_distance(geo, self._georef, 'epi', u)
        distendo = surf_distance(geo, self._georef, 'endo', u)

        return [ vol, self._volref, length, self._lengthref,
                 thick, self._thickref, distepi, distendo ]

if __name__ == "__main__" :

    if mpi_comm_world().rank == 0:
        set_log_level(PROGRESS)
    else:
        set_log_level(ERROR)

    datadir = "./data"
    meshdir = "./meshes_p1.00"
    outdir = "./results_sensitivity"
    case = "1024"

    #plist = [ 'a_iso' ]
    #pspan = np.linspace(0.1, 60.0, 150)
    #plist = [ 'b_iso' ]
    #pspan = np.linspace(0.1, 20.0, 40)
    #plist = [ 'a_fibers' ]
    #pspan = np.linspace(1.0, 400.0, 150)
    #plist = [ 'b_fibers' ]
    #pspan = np.linspace(0.1, 30.0, 40)

    plist = [ 'a_iso', 'a_fibers' ]
    pspan = [ np.hstack([ np.arange( 1.0,  10.0, 1.0),
                          np.arange(10.0,  30.0, 10.0) ]),
              np.hstack([ np.arange( 1.0,  10.0, 1.0),
                          np.arange(10.0, 100.0, 10.0) ]) ]

    # create the problem
    pbeval = ProblemEvaluator(meshdir, datadir, outdir, case, plist)
    pbeval.bootstrap(**{plist[i]: pspan[i][0] for i in range(0, len(plist))})

    # perform the simulations
    if len(plist) == 1 :
        res = np.zeros((plist[0].shape[0], 8))
        for i, val in np.ndenumerate(pspan) :
            res[i,:] = np.array(pbeval(**{plist[0]: val}))
    elif len(plist) == 2 :
        PSPAN = np.meshgrid(pspan[0], pspan[1])
        res = np.zeros((PSPAN[0].shape[0], PSPAN[0].shape[1], 8))
        for (i, j), _ in np.ndenumerate(PSPAN[0]) :
            val = { plist[0]: PSPAN[0][i,j], plist[1]: PSPAN[1][i,j] }
            res[i,j,:] = np.array(pbeval(**val))
            print res[i,j,:]

    # postprocess of the results (only serial)
    if MPI.size(mpi_comm_world()) == 1 :
        # txt files for LaTeX figures
        txtname = "{}/{}_{}.txt".format(outdir, case, "_".join(plist))
        pngname = "{}/{}_{}.png".format(outdir, case, "_".join(plist))
        if len(plist) == 1 :
            pass
        elif len(plist) == 2 :
            head = [ plist[1], plist[0], 'vol', 'volref', 'length', 
                     'lengthref', 'thick', 'thickref', 'distepi', 'distendo' ]
            with open(txtname, "w") as f :
                f.write(' '.join(head))
                f.write('\n')
                for p1, p2, r in zip(PSPAN[1], PSPAN[0], res) :
                    out = np.vstack([p1.ravel(), p2.ravel()] \
                        + [ r[:,k].ravel() for k in xrange(0, r.shape[1])])
                    np.savetxt(f, out.transpose(), delimiter=' ', fmt='%.3e')
                    f.write('\n')
        # generates png figures
        fig, axs = plt.subplots(2, 3)
        fig.set_size_inches(16., 8.)
        titles = [ 'V_EDP', 'Length', 'Thickness', 'Dist Epi', 'Dist Endo', '' ]
        target = [ res[0,0,1], 0.0, res[0,0,3], 0.0, res[0,0,5], 0.0, 0.0, 0.0 ]
        for ax, idx, label in zip(axs.ravel(), [0, 2, 4, 6, 7, -1], titles) :
            if idx == -1 : 
                ax.axis('off')
                continue
            ax.set_xlabel(plist[0])
            ax.set_title(label)
            if len(plist) == 1 :
                ax.plot(pspan[0], res[:,idx])
            elif len(plist) == 2 :
                cs1 = ax.contourf(PSPAN[0], PSPAN[1], res[:,:,idx], 50)
                cs2 = ax.contour(PSPAN[0], PSPAN[1], res[:,:,idx], 
                                 levels=[target[idx], target[idx]],
                                 colors='k', hold='on')
                ax.set_ylabel(plist[1])
                cbar = fig.colorbar(cs1, ax=ax, shrink=0.8)
        plt.subplots_adjust(left=0.04, right=0.98, hspace=0.30,
                            bottom=0.1, top=0.92, wspace=0.17)
        plt.show()
        fig.savefig(pngname, dpi=300)


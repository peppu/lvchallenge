from geometry import Geometry
from lvproblem import LVProblem
from fenics import *
import numpy as np
import cPickle

stages = ["DS", "ED", "ES"]

def load_problem(meshdir, outdir, case, stage, fibers_space = 'Quadrature_4') :
    # load parameters
    params = LVProblem.default_parameters()
    # geometry
    params['geometry']['meshdir'] = meshdir
    params['geometry']['case'] = case
    params['geometry']['stage'] = 'DS'
    params['geometry']['fibers_space'] = fibers_space
    # material
    params['material']['active_model'] = 'active_strain'
    params['material']['a_sheets'] = 0.0
    params['material']['b_sheets'] = 0.0
    params['material']['a_cross'] = 0.0
    params['material']['b_cross'] = 0.0
    # read from file
    cpname = "{}/{}_full_cycle_pv.cpickle".format(outdir, case)
    with open(cpname, "r") as f :
        data = cPickle.load(f)
        idx = data["stage_markers"][stage]
        for p in [ 'a_iso', 'b_iso', 'a_fibers', 'b_fibers', 'gamma_fibers' ] :
            pval = data[p][idx]
            params['material'][p].assign(pval)
        pendo = data['plist'][idx]

    # initialize the problem
    problem = LVProblem(**params)
    problem.set_control_mode('pressure')
    problem.set_pendo(pendo)

    # load the solution
    h5name = "{}/{}_full_cycle_u_save.h5".format(outdir, case)    
    if stage == "DS" :
        h5group = "/initial"
    else :
        h5group = "/state_{}".format(stage)
    h5file = HDF5File(mpi_comm_world(), h5name, "r")
    h5file.read(problem.state, h5group)

    return problem

def select_problem_stage(problem, outdir, stage) :
    # read from file
    case = problem.parameters['geometry']['case']
    cpname = "{}/{}_full_cycle_pv.cpickle".format(outdir, case)
    with open(cpname, "r") as f :
        data = cPickle.load(f)
        idx = data["stage_markers"][stage]
        for p in [ 'a_iso', 'b_iso', 'a_fibers', 'b_fibers', 'gamma_fibers' ] :
            pval = data[p][idx]
            problem.set_control_parameters(**{p: pval})
        pendo = data['plist'][idx]

    # initialize the problem
    problem.set_control_mode('pressure')
    problem.set_pendo(pendo)

    # load the solution
    h5name = "{}/{}_full_cycle_u_save.h5".format(outdir, case)    
    if stage == "DS" :
        h5group = "/initial"
    else :
        h5group = "/state_{}".format(stage)
    h5file = HDF5File(mpi_comm_world(), h5name, "r")
    h5file.read(problem.state, h5group)

    return problem

def load_field(datadir, meshdir, outdir,
               case, field, stages = [ "ED", "ES" ]) :
    
    # initialize the problem
    params = LVProblem.default_parameters()
    params["geometry"]["case"] = case
    params["geometry"]["meshdir"] = meshdir
    problem = LVProblem(**params)
    problem.set_control_mode("pressure")
    
    h5name = "{}/{}_full_cycle_u_save.h5".format(outdir, case)    
    if os.path.isfile(h5name):
        h5name = "{}/{}_full_cycle_u_save_running.h5".format(outdir, case)
        if os.path.isfile(h5name):
            error("The file \"{}\" does not exist.".format(h5name))
    cpname = "{}/{}_full_cycle_pv.cpickle".format(outdir, case)

    h5file = HDF5File(mpi_comm_world(), h5name, "r")
    h5file.read(problem.state, "/initial")

    with open(cpname, "r") as f :
        markers = cPickle.load(f)["stage_markers"]

    ret = []
    for stage in stages:
        tag = "values_{}".format(markers[stage])
        h5file.read(problem.state.vector(), tag, True)
        ret.append(problem.state.sub(field, deepcopy = True))

    return ret

def load_displacements(datadir, meshdir, outdir,
                       case, stages=["ED", "ES"]) :

    return load_field(datadir, meshdir, outdir, case, 0, stages)

def load_pressures(datadir, meshdir, outdir,
                  case, stages = [ "ED", "ES" ]) :
    
    return load_field(datadir, meshdir, outdir, case, 1, stages)

def load_material_parameters(outdir, case, stage) :
    cpname = "{}/{}_full_cycle_pv.cpickle".format(outdir, case)
    with open(cpname, "r") as f :
        p = cPickle.load(f)
    idx = p['stage_markers'][stage]
    return dict( (k, p[k][idx]) for k in [ 'a_iso', 'b_iso', 'a_fibers', 'b_fibers', \
                                           'gamma_fibers' ] if k in p)

def load_geometry_transformation(datadir, case):
    fname = '{}/raw_meshes/SCRIPT_OUTPUT2'.format(datadir)
    data = open(fname, 'r').readlines()
    idx = data.index('Process surface {}\n'.format(case))
    T = np.array([ map(float, s.replace(']', '').replace('TT= ', '')\
            .replace('[', '').strip().split()) for s in data[idx+2:idx+5] ])
    tra = float(data[idx+5].replace('T_trans= ', '').strip())
    b = np.array([tra, 0.0, 0.0])

    return T, b

def read_target_bc_dirichlet(case, datadir, stage = "ED") :

    fname = "{}/{}_InputData/"\
            "LV_BC/LV_Displacement_BC_{}.txt".format(datadir, case, stage)
    
    bcs = np.genfromtxt(fname, skiprows = 1,
                        dtype = [('id', 'int'), ('u', 'float', 3)])
    
    return bcs

def read_target_pressure(case, datadir, stage="ED"):
    assert stage in stages
    ind = stages.index(stage)
    p = np.genfromtxt("{}/{}_InputData/"\
                         "LV_BC/LV_Pressure.txt".format(datadir, case), usecols=(1,))[ind]
    return p

def read_target_volume(case, meshdir, stage="ED"):
    assert stage in stages
    geom = Geometry(case=case, meshdir=meshdir, stage=stage)
    return geom.inner_volume()
    
    #ind = stages.index(stage)
    #return np.genfromtxt("{}/{}_InputData/"\
    #                     "LV_BC/LV_Volume.txt".format(datadir, case), usecols=(1,))[ind]

def read_hexa_mesh_connectivity(case, datadir) :

    fname = "{}/{}_InputData/"\
            "LV_Fibre/ProcessedDTIData/LV_Hex_Mesh_Connectivity.txt"\
            .format(datadir, case)

    # read the file
    conn = np.loadtxt(fname, skiprows = 1, dtype = np.uintp)[:, 1:]
    # connectivity starts from 1, shift
    conn -= 1

    return conn

def read_hexa_mesh_xyz(case, datadir) :

    fname = "{datadir}/{case}_InputData/"\
            "LV_Fibre/ProcessedDTIData/DS_{case}_FibreVector.txt"\
            .format(datadir = datadir, case = case)

    # read the file
    return np.loadtxt(fname, skiprows = 1)[:, 1:4]

def read_hexa_mesh_fibers(case, datadir) :

    fname = "{datadir}/{case}_InputData/"\
            "LV_Fibre/ProcessedDTIData/DS_{case}_FibreVector.txt"\
            .format(datadir = datadir, case = case)

    # read the file
    return np.loadtxt(fname, skiprows = 1)[:, 4:]

def read_xyz_surface(case, stage, surf, datadir) :
    # reading data
    fname = '{}/{}_InputData/LV_Geometry/Geometry_Surface/{}_{}.txt'\
            .format(datadir, case, stage, surf)

    return np.loadtxt(fname, skiprows = 1)


from dataio import load_problem, select_problem_stage
from meshutils import surf_distance
from geometry import Geometry

def compare_geometries(meshdir, outdir, case, stage) :
    # load the problem
    problem = load_problem(meshdir, outdir, case, stage)
    # reference values
    meshdir = problem.parameters['geometry']['meshdir']
    case = problem.parameters['geometry']['case']
    georef = Geometry(meshdir=meshdir, case=case, stage=stage)

    ivolref = georef.inner_volume()
    wvolref = georef.wall_volume()
    lengthref = georef.ventricle_axial_length('epi')
    thickref = georef.average_radius('epi', 1.0) \
             - georef.average_radius('endo', 1.0)

    # actual values
    u = problem.state.sub(0, deepcopy=True)
    geo = problem.geo
    ivol = geo.inner_volume(u)
    wvol = geo.wall_volume(u)
    length = geo.ventricle_axial_length('epi', u)
    thick = geo.average_radius('epi', 1.0, u) \
          - geo.average_radius('endo', 1.0, u)

    # surface distance
    distepi  = surf_distance(geo, georef, 'epi', u)
    distendo = surf_distance(geo, georef, 'endo', u)

    labels = [ 'ivol', 'ivolref', 'wvol', 'wvolref', 'length', 'lengthref',
               'thick', 'thickref', 'distepi', 'distendo' ]
    res = [ ivol, ivolref, wvol, wvolref, length, lengthref,
            thick, thickref, distepi, distendo ]
    return dict(zip(labels, res))

def compile_qts(meshdir, outdir, case, stage) :
    qts = compare_geometries(meshdir, outdir, case, stage)
    ostr = "{} & {} & {:5.2f} & {:5.2f} & {:5.2f} & {:5.2f} & {:5.2f} & "\
           "{:5.2f} & {:5.2f} & {:5.2f}".format(\
           case if stage == 'ED' else '    ', stage,
           qts['thickref'], qts['thick'],
           qts['lengthref'], qts['length'],
           qts['wvolref'], qts['wvol'],
           qts['distendo'], qts['distepi'])

    return ostr

def compile_params(meshdir, outdir, case) :
    # load the problem
    problem = load_problem(meshdir, outdir, case, 'ED')
    ll = [ problem.get_control_parameter('a_iso'),
           problem.get_control_parameter('b_iso'),
           problem.get_control_parameter('a_fibers'),
           problem.get_control_parameter('b_fibers'),
           problem.get_control_parameter('gamma_fibers'),
           problem.get_control_parameter('pressure'),
           problem.get_control_parameter('pressure'),
           problem.get_control_parameter('volume'),
           problem.get_control_parameter('volume') ]
    # update ES values
    select_problem_stage(problem, outdir, 'ES')
    ll[4] = problem.get_control_parameter('gamma_fibers')
    ll[6] = problem.get_control_parameter('pressure')
    ll[8] = problem.get_control_parameter('volume')

    ostr = '{} & {:.3f} & {:.3f} & {:.3f} & {:.3f} & '\
           '{:.3f} & {:.1f} & {:.1f} & {:.2f} & {:.2f}'.format(case, *ll)
    return ostr

if __name__ == "__main__" :

    meshdir = "./meshes_p0.50"
    outdir  = "./results_p0.50"

    cases = [ '0912', '0917', '1017', '1024' ]
    stages = [ 'ED', 'ES' ]

    # table 1
    print(' \\\\[1ex]\n'.join(compile_params(meshdir, outdir, case)
                              for case in cases))

    print('\n\n')

    table2 = ''
    for case in cases :
        for stage in stages :
            space = "" if stage == stages[0] or case == cases[-1] else "[1ex]"
            table2 += compile_qts(meshdir, outdir, case, stage)
            table2 += ' \\\\{}\n'.format(space)
    print(table2)


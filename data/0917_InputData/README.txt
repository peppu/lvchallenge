InputData/

=> LV_Geometry/
	==> Geometry_Surface/
		===> DS_Endo.txt 		--> Endocardial surface points at diastasis (DS)
		===> DS_Epi.txt			--> Epicardial surface points at diastasis (DS)
		===> ED_Endo.txt		--> Endocardial surface points at end-diastole (ED)
		===> ED_Epi.txt			--> Epicardial surface points at end-diastole (ED)
		===> ES_Endo.txt		--> Endocardial surface points at end-systole (ES)
		===> ES_Epi.txt			--> Epicardial surface points at end-systole(ES)

	==> Geometry_BinaryImages/
		===> OutputImages_DS		--> Binary images for DS state
		===> OutputImages_ED		--> Binary images for ED state
		===> OutputImages_ES		--> Binary images for ES state

=> LV_Fibre/

	==> ProcessedDTIData/
		===> DS_0912_FibreVector.txt	--> Fibre vectors @ 3D spatial points embedded to DS geometry
		===> LV_Hex_Mesh_Connectivity.txt	--> Connectivity of refined hexahedral mesh

=> LV_BC/
	==> LV_Pressure.txt			--> LV cavity pressure at the end of each cardiac phases
	==> LV_Displacement_BC_ED.txt		--> Displacement boundary condition for the LV base from DS to ED
	==> LV_Displacement_BC_ES.txt		--> Displacement boundary condition for the LV base from ED to ES

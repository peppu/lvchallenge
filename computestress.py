from fenics import *
from vtk import *
from tvtk.array_handler import *
from lvsolver import LVSolver
from dataio import *
from meshutils import *
import numpy as np
import os.path
from scipy.spatial import cKDTree
from scipy.interpolate import Rbf

parameters['allow_extrapolation'] = True

def localproject(fun, V) :
    a = Form(inner(TestFunction(V), TrialFunction(V)) * dx)
    L = Form(inner(TestFunction(V), fun) * dx)
    res = Function(V)
    solver = LocalSolver()
    solver.solve(res.vector(), a, L)
    return res

def smooth_from_points(V, f0) :
    # points for f0
    V0 = f0.function_space()
    xyz = V0.dofmap().tabulate_all_coordinates(V0.mesh()).reshape(-1, 3)
    f0val = f0.vector().array()
    tree = cKDTree(xyz)

    # coordinate of the dofs
    coords = V.dofmap().tabulate_all_coordinates(V.mesh()).reshape(-1, 3)
    f = Function(V)

    nsamples = 10
    for idx in xrange(0, V.dim()) :
        v = coords[idx,:]
        samples_rad, samples_idx = tree.query(v, nsamples)
        xx, yy, zz = np.split(xyz[samples_idx,:], 3, axis=1)
        fvals = f0val[samples_idx]
        rbf = Rbf(xx, yy, zz, fvals, function='gaussian')
        f.vector()[idx] = float(rbf(v[0], v[1], v[2]))

    return f

def compute_stress(problem) :
    # displacement and pressure
    u = problem.state.sub(0, deepcopy=True)
    p = problem.state.sub(1, deepcopy=True)

    # actually 2nd order is optimal for P2, but we already have
    # fibers at quadrature points
    Vquad = FunctionSpace(problem.geo.domain, "Quadrature", 2)
    #Vquad = FunctionSpace(problem.geo.domain, "DG", 0)

    # deformation gradient tensor
    F = Identity(3) + grad(u)
    F = variable(F)
    J = det(F)
    Jm23 = pow(J, -float(2)/3)
    P = diff(problem.mat.strain_energy(F, p), F)

    # fibers
    f0 = problem.geo.f0

    # stress and strain along fibers
    Ifexpr = Jm23 * inner(F*f0, F*f0)
    Tfexpr = 1.0/J * 1.0/inner(F*f0, F*f0) * inner(P*F.T * (F*f0), F*f0)

    # project here is equivalent to interpolation
    Iffun = localproject(Ifexpr, Vquad)
    Tffun = localproject(Tfexpr, Vquad)

    # remove 'extreme' outliers
    Ilb, Iub = 0.0, 2.0
    Iffun.vector()[Iffun.vector().array() > Iub] = Iub
    Iffun.vector()[Iffun.vector().array() < Ilb] = Ilb
    print 'E[If] =', np.mean(Iffun.vector().array())
    print 'V[If] =', np.std(Iffun.vector().array())

    Tlb, Tub = -100.0, +500.0
    Tffun.vector()[Tffun.vector().array() > Tub] = Tub
    Tffun.vector()[Tffun.vector().array() < Tlb] = Tlb
    print 'E[Tf] =', np.mean(Tffun.vector().array())
    print 'V[Tf] =', np.std(Tffun.vector().array())

    # smoothing on P2 space
    VP2 = FunctionSpace(problem.geo.domain, "P", 2)
    If = smooth_from_points(VP2, Iffun)
    Tf = smooth_from_points(VP2, Tffun)

    return If, Tf

if __name__ == "__main__" :

    datadir = "./data"
    meshdir = "./meshes_p0.50"
    resdir  = "./results_p0.50"

    cases = [ '0912', '0917', '1017', '1024' ]
    stages = [ "ED", "ES" ]
    
    for case in cases :
        print "CASE", case
        # init problems
        problem = load_problem(meshdir, resdir, case, 'DS', 'Quadrature_2')
        ofile = File("{}_post.xdmf".format(case))
        for stage in stages :
            print "STAGE", stage
            select_problem_stage(problem, resdir, stage)
            I4, sigma = compute_stress(problem)
            ofile << Function(I4, name='If_{}'.format(stage))
            ofile << Function(sigma, name='Tf_{}'.format(stage))

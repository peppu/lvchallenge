from fenics import *
import numpy as np

# vtk is required only for post-processing
try :
    from vtk import *
    from tvtk.array_handler import *
    from meshutils import *
    has_vtk = True
    # check for old vtk
    from distutils.version import StrictVersion
    vtk6 = StrictVersion(vtkVersion.GetVTKVersion()) >= StrictVersion("6.0.0")
except ImportError :
    has_vtk = False

class Geometry() :
    def __init__(self, **kwargs) :
        # setup parameters
        p = self.default_parameters()
        p.update(**kwargs)
        self.parameters = p

        # load the mesh (by default in cm)
        h5name = "{meshdir}/{case}.h5".format(**p)
        h5file = HDF5File(mpi_comm_world(), h5name, "r")

        mesh_name = "{meshdir}/{case}{stage}.xml.gz".format(**p)
        self.mesh = Mesh()
        h5file.read(self.mesh, "{stage}/mesh".format(**p), False)
        self.domain = self.mesh.ufl_domain()

        # long axis
        self.parameters["long_axis"] = 0

        # boundary markers
        fgroup = '{stage}/facets markers'.format(**p)
        self.BASE = h5file.attributes(fgroup)['base']
        self.ENDO = h5file.attributes(fgroup)['endo']
        self.EPI  = h5file.attributes(fgroup)['epi']
        self.bfun = MeshFunction("size_t", self.mesh, 2)
        h5file.read(self.bfun, fgroup)

        rgroup = '{stage}/ridges markers'.format(**p)
        self.ENDORING = h5file.attributes(rgroup)['endoring']
        self.EPIRING  = h5file.attributes(rgroup)['epiring']
        self.rfun = MeshFunction("size_t", self.mesh, 1)
        h5file.read(self.rfun, rgroup)

        # fibers
        f0group = "{stage}/fibers at {fibers_space}".format(**p)
        if h5file.has_dataset(f0group) :
            family, degree = p['fibers_space'].split("_")
            V = VectorFunctionSpace(self.mesh, family, int(degree))
            self.f0 = Function(V)
            h5file.read(self.f0, f0group)
        else :
            self.f0 = None

    @staticmethod
    def default_parameters() :
        p = { 'meshdir' : './meshes',
              'case'    : '1024',
              'stage'   : 'DS',
              'long_axis' : 0,
              'fibers_space' : 'Quadrature_4' }
        return p

    def inner_volume(self, u = Constant((0.0, 0.0, 0.0))) :
        """
        Compute the inner volume of the cavity for a given displacement u.
        """

        # current inner volume
        X = SpatialCoordinate(self.domain)
        N = FacetNormal(self.domain)

        x = X + u
        F = grad(x)
        n = cofac(F) * N

        ds_endo = ds(self.ENDO, subdomain_data = self.bfun)

        Vendo_form = -1/float(3) * inner(x, n) * ds_endo

        return assemble(Vendo_form)

    def wall_volume(self, u = Constant((0.0, 0.0, 0.0))) :
        """
        Compute the volume of the wall.
        """

        X = SpatialCoordinate(self.domain)
        x = X + u
        F = grad(x)
        J = det(F)

        V_form = J * dx(domain = self.domain)

        return assemble(V_form)

    def ventricle_axial_length(self, surf, u = None) :
        """
        Compute the length of the ventricle by intersecting it with the
        line directed as its axis.
        """

        if not has_vtk :
            raise NotImplementedError

        # restrict on boundary
        marker = { "endo" : self.ENDO, "epi" : self.EPI }[surf]
        meshb = boundary_mesh(self.mesh, self.bfun, marker)
        if u :
            ub = restrict_on_boundary(meshb, u)
        else :
            ub = None

        # vtk grid
        grid = dolfin2vtk(meshb, ub)
        lingeo = linear_grid_filter(grid, 1)

        long_axis = self.parameters['long_axis']

        return lingeo.GetOutput().GetBounds()[2*long_axis + 1]

    def average_radius(self, surf, cut_quote, u = None) :
        """
        Compute average radius of an axial section of the ventricle.
        """

        if not has_vtk :
            raise NotImplementedError

        # restrict on boundary
        marker = { "endo" : self.ENDO, "epi" : self.EPI }[surf]
        meshb = boundary_mesh(self.mesh, self.bfun, marker)
        if u :
            ub = restrict_on_boundary(meshb, u)
        else :
            ub = None

        # vtk grid
        grid = dolfin2vtk(meshb, ub)
        lingeo = linear_grid_filter(grid, 1)

        # cut plane
        long_axis = self.parameters['long_axis']
        origin = [ 0.0, 0.0, 0.0 ]
        normal = [ 0.0, 0.0, 0.0 ]
        origin[long_axis] = cut_quote
        normal[long_axis] = 1.0
        plane = vtkPlane()
        plane.SetOrigin(*origin)
        plane.SetNormal(*origin)

        plane_cut = vtkCutter()
        if vtk6:
            plane_cut.SetInputData(lingeo.GetOutput())
        else:
            plane_cut.SetInput(lingeo.GetOutput())
            
        plane_cut.SetCutFunction(plane)
        plane_cut.Update()

        rsum = 0.0
        pts = plane_cut.GetOutput().GetPoints()
        for i in xrange(0, pts.GetNumberOfPoints()) :
            rsum += np.linalg.norm(pts.GetPoint(i)[1:])

        return rsum / pts.GetNumberOfPoints()

if __name__ == "__main__" :
    # tests
    meshdir = "./meshes_rev_fine"
    cases = [ "1024", "1017", "0912", "0917" ]
    stages = [ "DS", "ED", "ES" ]
    for case in cases :
        info("CASE {}".format(case))
        for stage in stages :
            info("  STAGE {}".format(stage))
            geo = Geometry(meshdir=meshdir, case=case, stage=stage)
            ivol = geo.inner_volume()
            wvol = geo.wall_volume()
            length_epi = geo.ventricle_axial_length('epi')
            length_endo = geo.ventricle_axial_length('endo')
            r_epi = geo.average_radius('epi', 1.0)
            r_endo = geo.average_radius('endo', 1.0)

            info("    INNER VOLUME = {}".format(ivol))
            info("    WALL VOLUME  = {}".format(wvol))
            info("    AXIAL LENGTH = {}".format(length_epi))
            info("    THICK BASE   = {}".format(r_epi-r_endo))
            info("    THICK APEX   = {}".format(length_epi-length_endo))


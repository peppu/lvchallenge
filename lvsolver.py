from dolfin import *

class LVSolver(object) :
    def __init__(self, use_snes=True, max_iterations=50) :
        self.reset(use_snes, max_iterations = max_iterations)

    def solve(self, problem) :
        problem._first_iteration = True
        problem._prev_residual = 1.
        problem._recompute_jacobian = True
        return self.solver.solve(problem, problem.state.vector())

    def reset(self, use_snes=True, max_iterations=50) :
        PETScOptions.set("pc_factor_mat_solver_package", "mumps")
        #PETScOptions.set("pc_factor_mat_solver_package", "superlu_dist")
        PETScOptions.set("ksp_type", "preonly")
        PETScOptions.set("mat_mumps_icntl_7", 6)
        if use_snes :
            solver = PETScSNESSolver()
            solver.parameters["report"] = False
            PETScOptions.set("snes_monitor")
        else :
            solver = NewtonSolver()
            
        solver.parameters["linear_solver"] = "lu"
        solver.parameters["maximum_iterations"] = max_iterations
        solver.parameters["lu_solver"]["same_nonzero_pattern"] = True
        solver.parameters["lu_solver"]["symmetric"] = True
        self.solver = solver


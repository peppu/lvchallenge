from fenics import *
from lvproblem import LVProblem
from itertarget import passive, active
from datacollector import DataCollector
import cPickle

if __name__ == "__main__" :
    if mpi_comm_world().rank == 0:
        set_log_level(PROGRESS)
    else:
        set_log_level(ERROR)
 
    datadir = "./data"
    meshcoarsedir = "./meshes_rev_1.00"
    rescoarsedir  = "./results_rev_1.00"
    meshfinedir = "./meshes_rev_0.75"
    resfinedir  = "./results_rev_0.75"

    #cases = [ "1017", "1024", "0912", "0917" ]
    cases = [ "1024" ]
    plist = [ "a_iso", "b_iso", "a_fibers", "b_fibers", "gamma_fibers" ]

    # common parameters
    params = LVProblem.default_parameters()
    # geometry
    params['geometry']['meshdir'] = meshfinedir
    params['geometry']['stage'] = 'DS'
    params['geometry']['fibers_space'] = 'Quadrature_4'
    # material
    params['material']['active_model'] = 'active_strain'
    params['material']['a_sheets'] = 0.0
    params['material']['b_sheets'] = 0.0
    params['material']['a_cross'] = 0.0
    params['material']['b_cross'] = 0.0
    params['material']['a_fibers'].assign(20.0)
    params['material']['a_iso'].assign(1.0)

    volume_control_parameters = [ 'a_fibers' ]

    for case in cases :
        # init the problem
        params['geometry']['case'] = case
        problem = LVProblem(**params)
        clname = "{}_full_cycle".format(case)
        collector = DataCollector(clname, problem, plist, resfinedir)

        # setup parameters
        cpname = "{}/{}_full_cycle_pv.cpickle".format(rescoarsedir, case)
        data = cPickle.load(open(cpname, "r"))
        
        # passive filling
        #pvals = { k: data[k][data['stage_markers']['ED']] for k in plist }
        #problem.set_control_parameters(**pvals)

        passive(problem, collector, volume_control_parameters, datadir)
        exit(0)

        # active contraction
        active(problem, collector, datadir)
        collector.move_files()


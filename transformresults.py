import numpy as np
from dataio import *
from meshutils import *
from geometry import Geometry
from computestress import compute_stress
from scipy.spatial import cKDTree
from scipy.interpolate import Rbf

parameters["allow_extrapolation"] = True

def kabsch(P, Q) :
    """
    Kabsch algorithm: given 2 paired datasets of points, it finds the optimal
    translation and rotation such that the distance between points is minimal.
    
    See: http://en.wikipedia.org/wiki/Kabsch_algorithm
    """

    # barycenter
    p0 = P.sum(0) / P.shape[0]
    q0 = Q.sum(0) / Q.shape[0]

    # translating to origin
    Pc = P - p0
    Qc = Q - q0

    # covariance matrix
    C = np.dot(Pc.T, Qc)

    # optimal rotation
    V, _, W = np.linalg.svd(C)
    d = np.sign(np.linalg.det(V.dot(W)))
    D = np.diag([1, 1, d])
    U = np.dot(W.T, np.dot(D, V.T))

    # translation
    r = q0 - U.dot(p0)

    # root mean square error
    diff = Pc.dot(U.T) - Qc
    rmse = np.sqrt((diff**2).sum(axis = 1).mean())

    return U, r, rmse

def fit_to_bcs(X, u, bcs) :
    # position of the deformed points (index starts from 1)
    ids = bcs['id'] - 1
    x_orig = X[ids, :] + u[ids, :]
    # target position
    x_target = X[ids, :] + bcs['u']
    # we find the optimal rigid transformation from our computed
    R, t, _ = kabsch(x_orig, x_target)
    # now the new map:
    # u = R * (X + u + t) - X
    #   = (R - I) * X + R * (u + t)
    u_optim = (X + u).dot(R.T) +  t - X
    # for debugging purpose, we compute the distance before and
    # after the transformation
    x_optim = X[ids, :] + u_optim[ids, :]
    d0 = np.sqrt(((x_orig - x_target)**2).sum(1).mean())
    d1 = np.sqrt(((x_optim - x_target)**2).sum(1).mean())
    print "BCs DISTANCE (before)", d0
    print "BCs DISTANCE (after) ", d1
    assert d0 >= d1

    return u_optim

def interpolate_meshless(oxyz, f0) :
    # points for f0
    V0 = f0.function_space()
    ixyz = V0.dofmap().tabulate_all_coordinates(V0.mesh()).reshape(-1, 3)
    f0val = f0.vector().array()
    if f0.value_rank() > 0 :
        idx = np.array([ V0.sub(i).dofmap().dofs() 
                         for i in range(0, V0.num_sub_spaces()) ]).transpose()
        ixyz = ixyz[idx[:,0],:]
        f0val = f0val[idx]

    tree = cKDTree(ixyz)

    # sampling
    of0 = []
    nsamples = 10
    for v in oxyz :
        samples_rad, samples_idx = tree.query(v, nsamples)
        xx, yy, zz = np.split(ixyz[samples_idx,:], 3, axis=1)
        if f0.value_rank() == 0 :
            fvals = f0val[samples_idx]
            rbf = Rbf(xx, yy, zz, fvals, function='gaussian')
            of0 += [ rbf(v[0], v[1], v[2]) ]
        else :
            fvals = f0val[samples_idx,:]
            rbf = [ Rbf(xx, yy, zz, ff, function='gaussian')
                    for ff in np.split(fvals, 3, axis=1) ]
            of0 += [ [ irbf(v[0], v[1], v[2]) for irbf in rbf ] ]

    return np.array(of0)

def transform_and_save(datadir, meshdir, outdir, case) :
    stages = [ 'ED', 'ES' ]
    # load the displacement and compute the fiber stress
    problem = load_problem(meshdir, outdir, case, 'DS', 'Quadrature_2')
    ulist, slist = [], []
    for stage in stages :
        select_problem_stage(problem, outdir, stage)
        ulist += [ problem.state.sub(0, deepcopy=True) ]
        slist += [ compute_stress(problem)[1] ]

    # coordinates of the hexahedral reference geometry
    conn = read_hexa_mesh_connectivity(case, datadir)
    X_hexa = read_hexa_mesh_xyz(case, datadir)
    X_hexa_idx = np.arange(1, X_hexa.shape[0]+1)[:, np.newaxis]

    # rotation and translation
    T, b = load_geometry_transformation(datadir, case)

    # transform coordinates
    scale = 10.0
    X = 1/scale * (X_hexa - b).dot(T.T)

    # target dirichlet bcs
    # bcs_es is relative to ED, but we need it relative to DS
    bcs_ed = read_target_bc_dirichlet(case, datadir, "ED")
    bcs_es = read_target_bc_dirichlet(case, datadir, "ES")
    bcs_es['u'] += bcs_ed['u']

    for stage, u, sigma, bcs in zip(stages, ulist, slist, [ bcs_ed, bcs_es ]) :
        # evaluate displacement on the hexahedral mesh and fit to bcs
        u_hexa = scale * interpolate_meshless(X, u).dot(T)
        #u_hexa = scale * np.apply_along_axis(u, 1, X).dot(T)
        u_hexa = fit_to_bcs(X_hexa, u_hexa, bcs)
        export_vtk("{}/{}{}_u_hexa.vtu".format(outdir, case, stage),
                   X_hexa, conn, u_hexa)
        #s_hexa = np.apply_along_axis(sigma, 1, X)
        s_hexa = interpolate_meshless(X, sigma)
        export_vtk("{}/{}{}_s_hexa.vtu".format(outdir, case, stage),
                   X_hexa, conn, s_hexa)

        # now save
        named = "{}/SG_{}_{}_MATLPTS.txt".format(outdir, case, stage)
        headd = "index x(mm) y(mm) z(mm) x_{stage}(mm) y_{stage}(mm) z_{stage}(mm)"\
                .format(stage = stage)
        np.savetxt(named, np.hstack((X_hexa_idx, X_hexa, X_hexa + u_hexa)),
                fmt = [ '%d', '%f', '%f', '%f', '%f', '%f', '%f' ],
                header = headd,
                comments = "")
        names = "{}/SG_{}_{}_FIBSTRESS.txt".format(outdir, case, stage)
        heads = "index x(mm) y(mm) z(mm) FIBSTRESS_{stage}(kPa)"\
                .format(stage = stage)
        np.savetxt(names, np.hstack((X_hexa_idx, X_hexa, s_hexa[:, np.newaxis])),
                fmt = [ '%d', '%f', '%f', '%f', '%f' ],
                header = heads,
                comments = "")

if __name__ == "__main__":

    datadir = "./data"
    meshdir = "./meshes_p0.50"
    outdir  = "./results_p0.50"

    cases = [ '0912', '0917', '1017', '1024' ]
    
    for case in cases :
        print "CASE", case
        transform_and_save(datadir, meshdir, outdir, case) 
